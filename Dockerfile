FROM ubuntu:18.10
RUN apt-get update && apt-get install --yes meld git adduser sudo && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN addgroup --gid 1000 username
RUN adduser --disabled-password --gecos "" --uid 1000 --gid 1000 username
CMD sudo -u username -H bash -c 'cd /mnt/ && git mergetool -y'
